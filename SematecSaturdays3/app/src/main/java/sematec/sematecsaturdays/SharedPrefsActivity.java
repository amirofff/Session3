package sematec.sematecsaturdays;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class SharedPrefsActivity extends AppCompatActivity {

    EditText name;
    EditText family;
    EditText age;

    ImageView pageImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefs);

        pageImage = (ImageView) findViewById(R.id.pageImage);


        Picasso.with(this).load("http://www.irib.ir/assets/news_images/20170607130643_6750.jpg").into(pageImage);


        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        age = (EditText) findViewById(R.id.age);

        name.setText(getShared("name", ""));
        family.setText(getShared("family", ""));
        age.setText(getShared("age", ""));


        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameValue = name.getText().toString();
                String familyValue = family.getText().toString();
                String ageValue = age.getText().toString();
//                int ageInt = Integer.parseInt(ageValue);
                saveShared("name", nameValue);
                saveShared("family", familyValue);
                saveShared("age", ageValue);
                showToast("data has been saved!");
                name.setText("");
                family.setText("");
                age.setText("");
            }
        });


    }


    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void saveShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(SharedPrefsActivity.this)
                .edit().putString(key, value).commit();
    }

    private String getShared(String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(SharedPrefsActivity.this)
                .getString(key, defaultValue);
    }


}
