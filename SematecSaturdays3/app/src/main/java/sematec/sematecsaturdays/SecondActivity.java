package sematec.sematecsaturdays;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    TextView studentName;
    Button btnShow;

    TextView resultFromMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        resultFromMain = (TextView) findViewById(R.id.resultFromMain);

        studentName = (TextView) findViewById(R.id.studentName);

        btnShow = (Button) findViewById(R.id.btnShow);

        studentName.setText("Amirhossein");

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentName.setText("Hello Worlddddddd");
                Toast.makeText(SecondActivity.this, "Clicked on BTN SHOW", Toast.LENGTH_LONG).show();
            }
        });


        btnShow.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(SecondActivity.this, "onLong Click", Toast.LENGTH_SHORT).show();


                return false;
            }
        });


        studentName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnShow.setText("Alireza");
            }
        });


        Intent mainIntent = getIntent();

        if( mainIntent.hasExtra("name")){
            String name = mainIntent.getStringExtra("name");
            int age = mainIntent.getIntExtra("age", 0);
            Toast.makeText(this, mainIntent.getStringExtra("name"), Toast.LENGTH_SHORT).show();
            resultFromMain.setText(name + " " + age);
        }



        try{

        }catch (Exception e){

        }


        String a = null ;

        Toast.makeText(this, a, Toast.LENGTH_SHORT).show();


    }
}
